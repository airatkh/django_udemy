from django.conf.urls import patterns, include, url
from article.api import ArticleResource

article_resource = ArticleResource()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'django_test.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^all/$', 'article.views.articles'),
    url(r'^get/(?P<article_id>\d+)/$', 'article.views.article'),
    url(r'^language/(?P<language>[a-z\-]+)/$', 'article.views.language'),
    url(r'^create/$', 'article.views.create'),
    url(r'^like/(?P<article_id>\d+)/$', 'article.views.like_article'),
    url(r'^add_comment/(?P<article_id>\d+)/$', 'article.views.add_comment'),
    url(r'^search/$', 'article.views.search_titles'),
    url(r'^api/', include(article_resource.urls)),

    # url(r'^hello_template/$', 'article.views.hello_template'),
    # url(r'^hello_template_simple/$', 'article.views.hello_template_simple'),
    # url(r'^hello_class_view/', HelloTemplate.as_view()),
    # url(r'^admin/', include(admin.site.urls)),
)
