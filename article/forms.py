from django import forms
from article.models import Article, Comment
#from django.db import models
#from djangomodels import Article
#from django.contrib.auth.forms import UserCreationForm


class ArticleForm(forms.ModelForm):

    class Meta:
        model = Article
        fields = {'title', 'body', 'pub_date', 'thumbnail'}
        #fields = {'title', 'body', 'pub_date'}


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = {'first_name', 'second_name', 'body'}