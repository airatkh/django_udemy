from django.db import models
from time import time
from django.conf import settings
#from whoosh import store, fields, index
from whoosh import fields, index
import os.path
from whoosh.index import  create_in, open_dir
from django_test import settings

def get_upload_file_name(instance, filename):
    #return "uploaded_files/%s_%s" % (str(time()).replace('.', '_'), filename)
    return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.', '_'), filename)

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=250)
    body = models.TextField()
    pub_date = models.DateTimeField('date published')
    like = models.IntegerField(default=0)
    thumbnail = models.FileField(upload_to=get_upload_file_name)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/articles/get/%i/" % self.id

    def get_thumnail(self):
        thumb = str(self.thumbnail)
        if not settings.DEBUG:
            thumb = thumb.replace('assets/', '')

        return thumb


class Comment(models.Model):
    #name = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200)
    second_name = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField('date published')
    article = models.ForeignKey(Article)