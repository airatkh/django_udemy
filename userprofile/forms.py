from django import forms
from userprofile.models import UserProfile


class UserProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        field = {'likes_cheese', 'favourite_hamster_name'}